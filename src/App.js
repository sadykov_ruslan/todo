import React ,{useState} from "react";
import './App.css';
import Form from "./Components/Form.js";
import List from "./Components/List.js";

function App() {
    const[inputText, setInputText] = useState("");
    const[todos, setTodos] = useState([]);
  return (
    <div className="App">
      <header className="App-header">
        <h1>To Do List</h1>
      </header>
        <Form
            todos={todos}
            setTodos={setTodos}
            inputText={inputText}
            setInputText={setInputText}/>
        <List
            todos={todos}
            setTodos={setTodos}
        />
    </div>
  );
}

export default App;
