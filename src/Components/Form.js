import React from "react";
import Todo from "./Todo";
const Form = ({inputText,setInputText,todos,setTodos}) => {
    const textValue = (e) => {
        setInputText(e.target.value);
    };
    const submitTodo = (e) => {
        e.preventDefault();
        setTodos([
            ...todos, {text: inputText, id: Math.random()*500}
        ]);
        setInputText('');
    }
    return(
        <form>
            <input value={inputText} onChange={textValue} type="text" className="todo-input"/>
            <button onClick={submitTodo} className="todo-button" type="submit">Add</button>
        </form>
    )
}

export default Form;