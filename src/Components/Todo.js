import React from "react";

const Todo = ({text,todos,setTodos,todo}) => {
    const deleting = () => {
    setTodos(todos.filter((el => el.id !== todo.id)));
    }
    return(
    <div className="todo">
        <li className="todo-item"> {text} </li>
        <button onClick={deleting} className='delete-btn'>X</button>
    </div>
    );
};

export default Todo;