import React from "react";
import Todo from "./Todo";

const List = ({todos,setTodos}) => {

    return(
        <div className="list-container">
            <ul className="list">
                {todos.map(todo => (
                    <Todo
                        text={todo.text}
                        key={todo.id}
                        todo={todo}
                        todos={todos}
                        setTodos={setTodos}
                    />
                ))}
            </ul>
        </div>
    )
}
export default List;